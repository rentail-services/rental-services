import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceService } from 'src/app/service.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  loginstatus: boolean;
  constructor(private router: Router, private service: ServiceService) { }

  ngOnInit() {

    this.service.profileLogged.subscribe((data) => {
      this.loginstatus = data;
      console.log(this.loginstatus)
    })

    if (localStorage.getItem('userId')) {
      this.loginstatus = true;
    }

  }
  postItem() {
    if (localStorage.getItem('userId')) {
      this.router.navigate(['/postproduct']);
    }
    else {
      this.router.navigate(['/login']);
    }
  }
}
