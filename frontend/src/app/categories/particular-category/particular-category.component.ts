import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ServiceService } from 'src/app/service.service';

@Component({
  selector: 'app-particular-category',
  templateUrl: './particular-category.component.html',
  styleUrls: ['./particular-category.component.css']
})
export class ParticularCategoryComponent implements OnInit {
  category: string;
  Itemsdata:any;
  constructor(private service: ServiceService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params => {
      this.category = params['category'];
    })

    this.service.getCategory(this.category).subscribe((data) => {
      this.Itemsdata=data;
      console.log(this.Itemsdata);
      
    })
  }

}
