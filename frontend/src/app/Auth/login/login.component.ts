import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ServiceService } from 'src/app/service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  email_status: string;
  password_status: string;

  constructor(private formBuilder: FormBuilder, private router: Router, private service: ServiceService) { }

  ngOnInit() {
    if (localStorage.getItem('userId')) {
      this.router.navigate(['/']);
    }

    this.registerForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(5)]]
    });
  }

  get f() { return this.registerForm.controls; }

  valuechange(newValue) {
    this.password_status = " ";
    this.email_status = " ";
  }
  onSubmit() {
    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
    }

    this.service.doLogin(this.registerForm.value).subscribe((data:any) => { 
      console.log(data)
      if (data.statusCode == 1) {
        this.service.isLoginForProfileNav(true);
        localStorage.setItem('userId', data.response.userId); 
        this.router.navigate(['/postproduct']);
      
      }
      else if (data.statusCode == 6) {
        this.email_status = "wrong Email";
      }
      else if (data.statusCode == 3) {
        this.password_status = "wrong password";
      }

    })

  }
}