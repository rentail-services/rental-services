import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './Home/home/home.component';
import { PostItemComponent } from './PostProduct/post-item/post-item.component';
import { LoginComponent } from './Auth/login/login.component';
import { RegistrationComponent } from './Auth/registration/registration.component';
import { DetailsOfProductComponent } from './categories/details-of-product/details-of-product.component';
import { ParticularCategoryComponent } from './categories/particular-category/particular-category.component';
import { FeedBackComponent } from './AboutApplication/feed-back/feed-back.component';
import { AutomobilesComponent } from './categories/automobiles/automobiles.component';
import { ElectronicsComponent } from './categories/electronics/electronics.component';
import { ProfileComponent } from './profile/profile.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'registration', component:RegistrationComponent}, 
  { path: 'postproduct', component: PostItemComponent },
  { path: 'automobiles', component: AutomobilesComponent },
  { path: 'electronics', component: ElectronicsComponent },
  { path: 'particularcategory', component: ParticularCategoryComponent },
  { path: 'detailsofproduct', component: DetailsOfProductComponent },
  { path: 'about', component: FeedBackComponent },
  { path: 'profile', component: ProfileComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
