import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  id: number = parseInt(localStorage.getItem('userId'));
  profile_data: any;
  userposts: any;
  isMypostsCollapsed: boolean = true;
  isWalletCollapsed: boolean = true;

  constructor(private service: ServiceService, private router: Router) { }

  ngOnInit() {
    this.service.getProfile(this.id).subscribe((data) => {
      this.profile_data = data;
      console.log(this.profile_data)
    })

    this.service.getUserPosts(this.id).subscribe((data) => {
      this.userposts = data;
      console.log(this.userposts)
    })
  }


  //logout function
  logout() { 
    localStorage.removeItem('userId'); 
    this.service.isLoginForProfileNav(false);
    this.router.navigate(['/']); 
  }
  
  //toggle for myposts
  MypostsCollapsed() {
    this.isMypostsCollapsed = !this.isMypostsCollapsed;
  }
  //toggle for  wallet
  WalletCollapsed() {
    this.isWalletCollapsed = !this.isWalletCollapsed;
  }
 
}
