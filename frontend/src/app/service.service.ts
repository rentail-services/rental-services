import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, isObservable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {
  url = 'http://192.168.150.61:8080/ciaorentals/';

  private profileNav = new BehaviorSubject<boolean>(false); 
  profileLogged = this.profileNav.asObservable();

  constructor(private http: HttpClient) { }

  isLoginForProfileNav(data: boolean) {
    this.profileNav.next(data);
  }

  //login function
  doLogin(data) {
    return this.http.post(this.url + 'user/login', data);
  }

  //get the items by category
  getCategory(data: string) {
    return this.http.get<any>(this.url + 'ItemByCategory/' + data);
  }

  //get the details by id
  getItemDetails(id: number) {
    return this.http.get<any>(this.url + 'searchItemById/' + id);
  }

  //get the user posts
  getUserPosts(id: number) {
    return this.http.get<any>(this.url + 'displayItems/' + id);
  }

  //get the user profile and posts
  getProfile(id: number) {
    return this.http.get<any>(this.url + 'user/profile/' + id);
  }
 // postitem  method
  doPost(data){
    return this.http.post<any>(this.url+'item/create',data);
  }

}
