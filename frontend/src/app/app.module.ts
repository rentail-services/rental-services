import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './Auth/login/login.component';
import { RegistrationComponent } from './Auth/registration/registration.component';
import { HeaderComponent } from './Header/header/header.component';
import { HomeComponent } from './Home/home/home.component';
 
import { ParticularCategoryComponent } from './categories/particular-category/particular-category.component';
import { DetailsOfProductComponent } from './categories/details-of-product/details-of-product.component';
import { PostItemComponent } from './PostProduct/post-item/post-item.component';
import { FeedBackComponent } from './AboutApplication/feed-back/feed-back.component';
import { AutomobilesComponent } from './categories/automobiles/automobiles.component';
import { ElectronicsComponent } from './categories/electronics/electronics.component';
import { FooterComponent } from './footer/footer.component';
import { ProfileComponent } from './profile/profile.component';
 
 

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegistrationComponent,
    HeaderComponent,
    HomeComponent, 
    ParticularCategoryComponent,
    DetailsOfProductComponent,
    PostItemComponent,
    FeedBackComponent,
    AutomobilesComponent,
    ElectronicsComponent,
    FooterComponent,
    ProfileComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
