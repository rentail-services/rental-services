import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceService } from 'src/app/service.service';

@Component({
  selector: 'app-post-item',
  templateUrl: './post-item.component.html',
  styleUrls: ['./post-item.component.css']
})
export class PostItemComponent implements OnInit {
  category: string = "";
  optionArray = [];
  brand: string;
  brand_error: string;
  model: string;
  model_error: string;
  rent_per_day: string;
  rent_per_day_error: string;
  place: string;
  place_error: string;
  text_pattern = /^[A-Za-z]+$/;
  phone_pattern = /^[0-9]{1,4}$/;

  constructor(private service: ServiceService) { }

  ngOnInit() {

  }

  changeBrand(event) {
    this.brand = event.target.value;
    console.log(this.brand);
    if (this.text_pattern.test(this.brand)) {
      console.log(this.brand);
    }
    else {
      this.brand_error = "Enter Valid Text"
    }

  }

  changeModel(event) {
    this.model = event.target.value;
    console.log(this.model);
    if (this.text_pattern.test(this.model)) {
      console.log(this.model);
    }
    else {
      this.model_error = "Enter Valid Text"
    }
  }

  changeRent(event) {
    this.rent_per_day = event.target.value;
    console.log(this.rent_per_day);
    if (this.phone_pattern.test(this.rent_per_day)) {
      console.log(this.rent_per_day);
    }
    else {
      this.rent_per_day_error = "Enter Valid Number"
    }
  }

  changePlace(event) {
    this.place = event.target.value;
    console.log(this.place);
    if (this.text_pattern.test(this.place)) {
      console.log(this.place);
    }
    else {
      this.place_error = "Enter Valid Number"
    }
  }

  saverange() {
    console.log("data")
    if (this.category == "automobiles") {
      this.optionArray = ["select automobiles", "bike", "car", "trucks"];
    }
    else if (this.category == "electronics") {
      this.optionArray = ["select electronics", "mobile", "laptop", "refrigerator", "sofa", "washingmachine", "camera"];
    }
    console.log(this.optionArray)
  }
  onSubmit(data: any) {
    
    data.user = {
      id: parseInt(localStorage.getItem('userId'))
    }
    this.service.doPost(data).subscribe((data: any) => {
      console.log(data);
      if (data.statusCode == 1) {
        localStorage.setItem('post-item-statuscode', data.statusCode);
      }
    })
    console.log(data)
  }
}
