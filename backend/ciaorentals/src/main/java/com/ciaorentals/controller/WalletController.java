package com.ciaorentals.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ciaorentals.entities.TransactionEntity;
import com.ciaorentals.entities.WalletEntity;
import com.ciaorentals.service.TrasanctionServiceImpl;
import com.ciaorentals.service.WalletServiceImpl;
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/ciaorentals")
public class WalletController {
	@Autowired
	WalletServiceImpl walletDetails;
	@Autowired
	TrasanctionServiceImpl transactionDetails;
	/**
	 * This method is used to create the wallet to the particular user.
	 * @param walletMoney
	 * @return
	 */
	@PostMapping("/createWallet")
	public ResponseEntity<WalletEntity> createwallet(@RequestBody WalletEntity walletMoney) {

		WalletEntity wallet1 = walletDetails.createWallet(walletMoney);
		if (wallet1 != null) {
			return new ResponseEntity<WalletEntity>(wallet1, HttpStatus.OK);
		}  

		return new ResponseEntity<WalletEntity>(HttpStatus.NO_CONTENT);

	}
	/**
	 * This method is used to add money to wallet
	 * @param walletMoney
	 * @return
	 */
	@PostMapping("/addMoney")
	public ResponseEntity<String> addmoneyTowallet(@RequestBody WalletEntity walletMoney) {

		String wallet1 = walletDetails.addMoney(walletMoney);
		if (wallet1.equals("1")) {
			
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"added money to wallet\",\"statuscode\":" + 200 + "}");
		                    }
		else if (wallet1.equals("2")) {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"cannot add money reached your  limit \",\"statuscode\":" + 200 + "}");
		                    }
		 else if(wallet1.equals("-1")) {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"user already exists with this id\",\"statuscode\":" + 201 + "}");

	                        }
		return ResponseEntity.status(HttpStatus.OK)
				.body("{\"status\":\"wallet dosent exist\",\"statuscode\":" + 202 + " }");
	 }
	/**
	 * This method is used to debit amount from the table 
	 * @param transaction
	 * @return
	 */
	@PostMapping("/tranasaction_details")
	public ResponseEntity<TransactionEntity> transactionDetails(@RequestBody TransactionEntity transaction) {
		TransactionEntity trans = transactionDetails.debitMoney(transaction);
		if (trans != null) {
			return new ResponseEntity<TransactionEntity>(trans, HttpStatus.OK);
		}  

		return new ResponseEntity<TransactionEntity>(HttpStatus.NO_CONTENT);

	}
}