package com.ciaorentals.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ciaorentals.entities.ItemEntity;
import com.ciaorentals.service.ItemServiceImpl;
import com.ciaorentals.util.ResponseType;
import com.ciaorentals.util.StatusCodes;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/ciaorentals")
public class ItemController {

	@Autowired
	private ItemServiceImpl itemServiceImpl;
	@Autowired
	ResponseType responseType;

	/**
	 * This method is used to create the item for the particular user.
	 * 
	 * @param itemEntity
	 * @return
	 */
	@PostMapping("/item/create")
	public ResponseEntity<?> createNewAccount(@RequestBody ItemEntity itemEntity) {
		if(itemEntity != null) {
		ItemEntity item = itemServiceImpl.save(itemEntity);
		responseType.setResponse(StatusCodes.OK);
		responseType.setStatusCode(StatusCodes.OK.getValue());
		return new ResponseEntity<>(item, HttpStatus.OK);
		}else {
			responseType.setResponse(StatusCodes.NOT_AUTH);
			responseType.setStatusCode(StatusCodes.NOT_AUTH.getValue());
			return new ResponseEntity<>(responseType, HttpStatus.OK);
		}
	}

	@GetMapping("/displayItems/{user_id}")
	public ResponseEntity<List<ItemEntity>> getById(@PathVariable("user_id") int id) {

		List<ItemEntity> items = itemServiceImpl.getItemDetails(id);
		if (items != null) {
			return new ResponseEntity<List<ItemEntity>>(items, HttpStatus.OK);
		}

		return new ResponseEntity<List<ItemEntity>>(HttpStatus.NO_CONTENT);

	}

	@PutMapping("/updateItem/{item_id}")
	public ResponseEntity<ItemEntity> getByItemId(@PathVariable("item_id") int id, @RequestBody ItemEntity item) {

		ItemEntity items = itemServiceImpl.getByItemId(id, item);
		if (items != null) {
			return new ResponseEntity<ItemEntity>(items, HttpStatus.OK);
		}

		return new ResponseEntity<ItemEntity>(HttpStatus.NO_CONTENT);

	}
	/**
	 * This method is used to get the item object by category.
	 * @param type
	 * @return
	 */
	@GetMapping("/ItemByCategory/{type}")
	public ResponseEntity<List<ItemEntity>> searchByCategory(@PathVariable("type") String type) {

		List<ItemEntity> items = itemServiceImpl.filterBycategory(type);
		if (items != null) {
			return new ResponseEntity<List<ItemEntity>>(items, HttpStatus.OK);
		}
		
		return new ResponseEntity<List<ItemEntity>>(HttpStatus.NO_CONTENT);
	}

	/**
	 * this methods calls the service package for displaying the Items based on
	 * itemId
	 * 
	 * @param id
	 * @return
	 */
	@GetMapping("/searchItemById/{id}")
	public ResponseEntity<ItemEntity> getByItemId(@PathVariable("id") int id) {
		ItemEntity itemEntity = itemServiceImpl.filterById(id);
		if (itemEntity == null) {

			return new ResponseEntity<ItemEntity>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<ItemEntity>(itemEntity, HttpStatus.OK);
	}

	/**
	 * this methods calls the service package for deleting the items based on id
	 * 
	 * @param id
	 * @return
	 */
	@DeleteMapping("/items/delete/{id}")
	public ResponseEntity<?> delete(@PathVariable("id") int id) {
		ItemEntity itemEntity = itemServiceImpl.filterById(id);
		if (itemEntity != null) {
			itemServiceImpl.deleteItem(id);
			responseType.setResponse(StatusCodes.OK);
			responseType.setStatusCode(StatusCodes.OK.getValue());
			return new ResponseEntity<>(responseType, HttpStatus.OK);

		} else {
			responseType.setResponse(StatusCodes.NOT_FOUND);
			responseType.setStatusCode(StatusCodes.NOT_FOUND.getValue());
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		}

	}
}
