package com.ciaorentals.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ciaorentals.beans.UserBasicDto;
import com.ciaorentals.entities.UserEntity;
import com.ciaorentals.service.UserServiceImpl;

/**
 * This class implements the user
 * 
 * @author IMVIZAG
 *
 */
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/ciaorentals")
public class UserController {

	@Autowired
	private UserServiceImpl userServiceImpl;

	/**
	 * This method is used to register the user into user_entity table.
	 * 
	 * @param user
	 * @return
	 */
	@PostMapping("/user/register")
	public ResponseEntity<UserEntity> registerUser(@RequestBody UserEntity user) {
		UserEntity users = userServiceImpl.userRegistration(user);
		if (user == null) {
			return new ResponseEntity<UserEntity>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<UserEntity>(users, HttpStatus.OK);
	}

	/**
	 * This method is used to login the user
	 * 
	 * @param userEntity
	 * @return
	 * @throws Exception
	 */
	@PostMapping("/user/login")
	public ResponseEntity<String> loginUser(@RequestBody UserEntity userEntity) throws Exception {

		UserBasicDto userLogin = userServiceImpl.Login(userEntity);
		if (userLogin != null) {
			return ResponseEntity.status(HttpStatus.OK).body("{\"status\":\"success\",\"statuscode\":" + 200 + "}");
		}
		return ResponseEntity.status(HttpStatus.OK).body("{\"status\":\"failed\",\"statuscode\":" + 200 + "}");
	}

}
