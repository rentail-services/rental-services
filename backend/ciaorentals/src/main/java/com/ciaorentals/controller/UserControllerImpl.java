package com.ciaorentals.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ciaorentals.beans.UserBasicDto;
import com.ciaorentals.entities.FeedBack;
import com.ciaorentals.entities.UserEntity;
import com.ciaorentals.service.UserServiceImpl;
import com.ciaorentals.util.ResponseType;
import com.ciaorentals.util.StatusCodes;

/**
 * This class implements the user Operations.
 * 
 * @author IMVIZAG
 *
 */
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/ciaorentals")
public class UserControllerImpl {
	@Autowired
	UserServiceImpl userService;
	@Autowired
	ResponseType responseType;
	@Autowired
	UserServiceImpl service;

	/**
	 * This method is used to register the user into user_entity table.
	 * 
	 * @param user
	 * @return
	 */
	@SuppressWarnings("unused")
	@PostMapping("/user/register")
	public ResponseEntity<?> registerCandidate(@RequestBody UserEntity user) {
		ResponseEntity<ResponseType> responseEntity = null;
		boolean isexisted = userService.isExisted(user.getEmail());
		if (isexisted) {
			responseType.setResponse("User already exists with this email ");
			responseType.setStatusCode(StatusCodes.ALREADY_EXIST.getValue());
			responseEntity = new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
			return responseEntity;
		}
		UserEntity users = userService.userRegistration(user);
		if (user == null) {

			responseType.setStatusCode(StatusCodes.NOT_REGISTER.getValue());
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		} else {
			responseType.setResponse(user);
			responseType.setStatusCode(StatusCodes.OK.getValue());
			return new ResponseEntity<>(responseType, HttpStatus.OK);

		}
	}

	/**
	 * This method is used to login the user
	 * 
	 * @param userEntity
	 * @return
	 * @throws Exception
	 */
	@PostMapping("/user/login")
	public ResponseEntity<ResponseType> LoginCandidate(@RequestBody UserEntity userDetails) {
		try {
			UserBasicDto userBasic = userService.Login(userDetails);

			if (userBasic != null) {
				responseType.setResponse(userBasic);
				responseType.setStatusCode(StatusCodes.OK.getValue());
				return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);

			} else {
				responseType.setResponse(StatusCodes.NOT_FOUND);
				responseType.setStatusCode(StatusCodes.NOT_FOUND.getValue());
				return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseType.setResponse(e.getMessage());
			responseType.setStatusCode(StatusCodes.NOT_REGISTER.getValue());
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		}

	}

	/**
	 * This method is used to display all user details by UserId.
	 * 
	 * @param id
	 * @return
	 */
	@GetMapping("/user/profile/{id}")
	public ResponseEntity<UserEntity> UserProfiles(@PathVariable("id") int id) {
		UserEntity userEntity = userService.filterById(id);
		if (userEntity == null) {

			return new ResponseEntity<UserEntity>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<UserEntity>(userEntity, HttpStatus.OK);
	}

	@PostMapping("/giveFeedBack/{id}")

	public ResponseEntity<ResponseType> giveFeedBack(@PathVariable("id") int id, @RequestBody FeedBack feedback) {

		UserEntity userEntity = userService.filterById(id);
		if (userEntity == null) {

			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		FeedBack status = service.giveFeedBack(id, feedback);

		if (status != null) {
			responseType.setResponse(StatusCodes.OK);
			responseType.setStatusCode(StatusCodes.OK.getValue());
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		} else {
			responseType.setResponse(StatusCodes.ERROR);
			responseType.setStatusCode(StatusCodes.ERROR.getValue());
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		}
	}
	/**
	 * This method is used to display the all feedback details for a particular userId.
	 * @param id
	 * @return
	 */
	@GetMapping("/user/getfeedbacklist/{id}")
	
	public ResponseEntity<?> feedbacklists(@PathVariable("id") int id){
		List<FeedBack> userEntity = userService.list(id);
		if (userEntity == null) {

			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(userEntity, HttpStatus.OK);
	}
}
