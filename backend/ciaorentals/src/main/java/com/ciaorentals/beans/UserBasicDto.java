package com.ciaorentals.beans;
/**
 * This is UserEntity Object to display the mandatory fields.
 * @author IMVIZAG
 *
 */
public class UserBasicDto {

	private int userId;
	private String UserName;
	private String email;
	private String image;
	
	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return UserName;
	}

	public void setUserName(String UserName) {
		this.UserName = UserName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

}
