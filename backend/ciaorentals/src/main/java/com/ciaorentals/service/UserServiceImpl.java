package com.ciaorentals.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ciaorentals.beans.UserBasicDto;
import com.ciaorentals.dao.FeedBackDAO;
import com.ciaorentals.dao.ImagesDao;
import com.ciaorentals.dao.ItemDao;
import com.ciaorentals.dao.UserDao;
import com.ciaorentals.entities.FeedBack;
import com.ciaorentals.entities.ImagesEntity;
import com.ciaorentals.entities.UserEntity;
import com.ciaorentals.util.ModelMapperUtil;

/**
 * This class is used to implements the user service.
 * 
 * @author IMVIZAG
 *
 */
@Service
public class UserServiceImpl implements Userservice {
	@Autowired
	UserDao userDao;
	@Autowired
	ItemDao itemDao;
	@Autowired
	ImagesDao imageDao;
	@Autowired
	private ModelMapperUtil modelMapperUtil;
	@Autowired
	private FeedBackDAO feedbackDAO;

	/**
	 * This method is used to register the user in userEntity table
	 */
	@Override
	public UserEntity userRegistration(UserEntity user) {
		
		user.setImages(user.getImages().stream().map(image-> imageDao.save(image)).collect(Collectors.toList()));
		userDao.save(user);
		return user;
	}
//
//	public ImagesEntity saveImage(ImagesEntity image) {
//		return imageDao.save(image);
//	}

	/**
	 * This method is used to verify the user registration password and with his
	 * password
	 */
	@Override
	public UserBasicDto Login(UserEntity userLogin) throws Exception {

		UserEntity user = userDao.findByEmail(userLogin.getEmail());

		UserBasicDto userBasic = null;

		if (user != null) {
			userBasic = modelMapperUtil.convertEntityToDto(user, UserBasicDto.class);
		} else {
			throw new Exception("Not a Registered user");
		}
		if (userBasic != null) {

			if (user.getPassword().equals(userLogin.getPassword())) {

				return userBasic;
			}
		}
		return null;
	}

	/**
	 * this method is used to verify the email whether it is existed or not
	 */
	@Override
	public boolean isExisted(String email) {

		UserEntity user = userDao.findByEmail(email);
		return user != null;

	}

	/**
	 * This method is used to get the user object.
	 * 
	 * @param id
	 * @return
	 */
	@Override
	public UserEntity filterById(int id) {
		UserEntity users = userDao.findById(id);
		return users;
	}

	/**
	 * User can give feedback to the application.
	 */
	@Override
	public FeedBack giveFeedBack(int userId, FeedBack feedBack) {
		UserEntity users = userDao.findById(userId);
		feedBack.setUser(users);
		return feedbackDAO.save(feedBack);
	}

	@Override
	public List<FeedBack> list(int userId) {
		List<FeedBack> feedback = feedbackDAO.feedbacklist(userId);
		return feedback;
	}

	

}
