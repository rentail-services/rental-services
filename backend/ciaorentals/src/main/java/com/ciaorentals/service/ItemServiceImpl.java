package com.ciaorentals.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ciaorentals.dao.ItemDao;
import com.ciaorentals.dao.TransactionDao;
import com.ciaorentals.dao.UserDao;
import com.ciaorentals.entities.ItemEntity;
import com.ciaorentals.entities.TransactionEntity;

@Service
@Transactional
public class ItemServiceImpl implements ItemService {

	@Autowired
	private ItemDao itemDao;
	
	@Autowired
	private UserDao userDao;
	
	@Autowired
	TransactionDao transactionDao;

	@Override
	public ItemEntity save(ItemEntity itemEntity) {
		
		return itemDao.save(itemEntity);
	}
	public List<ItemEntity> getItemDetails(int id) {

		List<ItemEntity> items=itemDao.findByUserId(id);
		int temp = 0;
		for(ItemEntity itemDetails:items) {
		temp=temp+itemDetails.getRentPerDay();
		}
		
		TransactionEntity transactions = transactionDao.findByUser(id);
		if(transactions!=null) {
			
		}
		return items;
	}
	
	  
	
	public ItemEntity getByItemId(int id,ItemEntity item) {
		ItemEntity itemDetails=itemDao.findById(id);
		if(item.getBrand()!=null) {
		itemDetails.setBrand(item.getBrand());
		}

		if(item.getCategory()!=null) {
		itemDetails.setCategory(item.getCategory());
		}
		if(item.getModel()!=null) {
		itemDetails.setModel(item.getModel());
		}
		if(item.getPlace()!=null) {
		itemDetails.setPlace(item.getPlace());
		}
		if(item.getImages()!=null) {
		itemDetails.setImages(item.getImages());
		}
		if(item.getAvailableUpTo()!=null) {
		itemDetails.setAvailableUpTo(item.getAvailableUpTo());
		}
		return itemDao.save(itemDetails);
		
	}

	/**
	 * This method is used to display the Item details by Item Category.
	 */
	public List<ItemEntity> filterBycategory(String type) {
		  List<ItemEntity> items= itemDao.findByCategory(type);
		return items;
	}
	/**
	 * This method is used to display the all user details by userId.
	 */
	@Override
	public ItemEntity filterById(int id) {
		ItemEntity items = itemDao.findById(id);
		return items;
	}
	/**
	 * this method calls the DAO package for deleting the cultural events
	 */
	@Override
	public boolean deleteItem(int id) {		
		ItemEntity itemEntity = itemDao.findById(id);
		if (itemEntity != null) {
			itemDao.delete(itemEntity);
			return true;
		}
		return false;
	}


}

