package com.ciaorentals.service;


import com.ciaorentals.entities.WalletEntity;

public interface WalletService {
	
	public String addMoney(WalletEntity walletMoney);
	
	public WalletEntity createWallet(WalletEntity walletMoney);
	
//	public List<TransactionEntity> transactionHistory(int walletid);
}
