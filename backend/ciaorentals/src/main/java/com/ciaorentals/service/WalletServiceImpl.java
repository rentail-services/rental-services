package com.ciaorentals.service;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ciaorentals.dao.ItemDao;
import com.ciaorentals.dao.TransactionDao;
import com.ciaorentals.dao.UserDao;
import com.ciaorentals.dao.WalletDao;
import com.ciaorentals.entities.TransactionEntity;
import com.ciaorentals.entities.WalletEntity;

@Service
public class WalletServiceImpl implements WalletService{

	@Autowired
	UserDao userDao;
	@Autowired
	WalletDao walletDao;
	@Autowired
	ItemDao itemDao;
	@Autowired
	TransactionDao transactionDao;



	public WalletEntity createWallet(WalletEntity walletMoney) {

		return walletDao.save(walletMoney);
	}

	@Override
	public String addMoney(WalletEntity walletMoney) {
		try {

			WalletEntity wallet = walletDao.findById(walletMoney.getId());
			if (wallet != null) {
				if (wallet.getAmount() < 10000 && walletMoney.getAmount() < 10000) {
					wallet.setAmount(wallet.getAmount() + walletMoney.getAmount());
					walletDao.save(wallet);
					
					return "1";
				}
				
				return "2";
			}
		} catch (Exception e) {
			return "0";
		}
		return "-1";

	}


//	@Override
//	public List<TransactionEntity> transactionHistory(int walletid) {
//		
//		return new transactionDao().;
//	}
}
