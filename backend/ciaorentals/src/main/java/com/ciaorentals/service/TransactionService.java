package com.ciaorentals.service;

import com.ciaorentals.entities.TransactionEntity;
/**
 * This interface contains Transaction details of wallet.
 * @author IMVIZAG
 *
 */
public interface TransactionService {
	/**
	 * This method is used to debit the amount from the wallet
	 * @param transactions
	 * @return
	 */
	public TransactionEntity debitMoney(TransactionEntity transactions);
}
