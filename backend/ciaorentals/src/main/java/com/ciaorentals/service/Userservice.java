package com.ciaorentals.service;

import java.util.List;

import com.ciaorentals.beans.UserBasicDto;
import com.ciaorentals.entities.FeedBack;
import com.ciaorentals.entities.UserEntity;

/**
 * This interface is used to perform all the user functionalities.
 * @author IMVIZAG
 *
 */
public interface Userservice {
	/**
	 * This method is used to register the user to the UserEntity table.
	 * 
	 * @param user
	 * @return
	 */
	UserEntity userRegistration(UserEntity user);

	/**
	 * This method is used to check whether the email is existing or not.
	 * 
	 * @param email
	 * @return
	 */
	public boolean isExisted(String email);

	/**
	 * This method is used to login the user.
	 * 
	 * @param userLogin
	 * @return
	 * @throws Exception
	 */
	UserBasicDto Login(UserEntity userLogin) throws Exception;

	/**
	 * This method is used to get the user object.
	 * 
	 * @param id
	 * @return
	 */
	public UserEntity filterById(int id);

	/**
	 * This method is used to give the feedback.
	 * 
	 * @param userId
	 * @param feedBack
	 * @return
	 * @throws Exception
	 */
	public FeedBack giveFeedBack(int userId, FeedBack feedBack);

	/**
	 * This method is used to display all feedback list.
	 * 
	 * @param feedBackDetails
	 * @return
	 */
	public List<FeedBack> list(int userId);
}
