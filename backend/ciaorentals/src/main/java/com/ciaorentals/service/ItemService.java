package com.ciaorentals.service;

import java.util.List;

import com.ciaorentals.entities.ItemEntity;

public interface ItemService {

	public ItemEntity save(ItemEntity itemEntity);

	public List<ItemEntity> getItemDetails(int id);

	public ItemEntity getByItemId(int id, ItemEntity item);

	public List<ItemEntity> filterBycategory(String type);

	public ItemEntity filterById(int id);
	
	public boolean deleteItem(int id);

}
