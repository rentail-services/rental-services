package com.ciaorentals.util;

import org.springframework.stereotype.Component;

/**
 * this  class is used to set the response,statuscode and token
 * @author IMVIZAG
 *
 */
@Component
public class ResponseType {
	
	private Object response;
	private int statusCode;


	
	
	public Object getResponse() {
		return response;
	}
	public void setResponse(Object response) {
		this.response = response;
	}
	public int getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
} 
