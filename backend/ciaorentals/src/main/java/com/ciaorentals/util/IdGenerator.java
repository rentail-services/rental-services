package com.ciaorentals.util;

import java.io.Serializable;

import java.sql.Connection;
import java.sql.ResultSet;

import java.sql.Statement;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;

/**
 * this class is used to create a auto generated values for the all auto increment variables
 * @author IMVIZAG
 *
 */

public class IdGenerator implements IdentifierGenerator {
	
	@Override
	public Serializable generate(SharedSessionContractImplementor session, Object object) throws HibernateException {
		
		Connection con = session.connection();
		Integer newId = 0;
		ResultSet rs = null;
		Statement st = null;
		int prefix = 0;
		try {
			st = con.createStatement();
			switch (object.getClass().getName()) {
			case "com.ciaorentals.entities.UserEntity":
				prefix = 1001;
				rs = st.executeQuery("select max(id) as max from user_entity");

				break;
				
			case "com.ciaorentals.entities.ItemEntity":
				prefix = 2001;
				rs = st.executeQuery("select max(id) as max from items_entity");
				break;
				
			case "com.ciaorentals.entities.WalletEntity":
				prefix = 3001;
				rs = st.executeQuery("select max(id) as max from wallet_entity");
				break;
				
			case "com.ciaorentals.entities.TransactionEntity":
				session.flush();
				prefix = 4001;
				rs = st.executeQuery("select max(id) as max from transaction_entity");
				break;
			case "com.ciaorentals.entities.FeedBack":
				prefix = 5001;
				rs = st.executeQuery("select max(feedback_id) as max from feedback_tbl");
				break;
				
			case "com.ciaorentals.entities.ImagesEntity":
				session.flush();
				prefix = 6001;
				rs = st.executeQuery("select max(image_id) as max from  image_tbl");
				break;
			default:
				break;
			}

			if (rs.next()) {
				newId = rs.getInt("max") == 0 ? rs.getInt("max") + prefix : rs.getInt("max") + 1;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return newId;
	}

}
