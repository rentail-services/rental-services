package com.ciaorentals;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.ciaorentals.util.ModelMapperUtil;

@SpringBootApplication
public class CiaorentalsApplication {

	public static void main(String[] args) {
		SpringApplication.run(CiaorentalsApplication.class, args);
	}
	
	@Bean
	public ModelMapper getMapper() {
		return new ModelMapper();
	}

}
