package com.ciaorentals.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
/**
 * This class implements the feedback details.
 * @author ciaorentals
 *
 */
@Entity
@Table(name = "feedback_details_tbl")
public class FeedBackDetails {
	//feedback table fields and setters and getters are initialized.
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int feedback_details_id;
	
	@Column
	private String comment;

	@ManyToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "feed_back_feedback_id")
	private FeedBack feedBack;

	public FeedBack getFeedBack() {
		return null;
	}
	public void setFeedBack(FeedBack feedBack) {
		this.feedBack = feedBack;
	}
	public int getFeedback_details_id() {
		return feedback_details_id;
	}
	public void setFeedback_details_id(int feedback_details_id) {
		this.feedback_details_id = feedback_details_id;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}

}
