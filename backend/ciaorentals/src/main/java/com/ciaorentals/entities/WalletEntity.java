package com.ciaorentals.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "wallet_entity")
public class WalletEntity {

	@Id
	@GenericGenerator(name = "IdGen" ,strategy = "com.ciaorentals.util.IdGenerator")
	@GeneratedValue(generator = "IdGen")
	@Column(name = "id")
	private int id;
	@Column(name = "amount")
	private double amount;
	@OneToOne
	private UserEntity user;
	
	@OneToMany
	private List<TransactionEntity> transactions;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	public List<TransactionEntity> getTransactions() {
		return transactions;
	}

	public void setTransactions(List<TransactionEntity> transactions) {
		this.transactions = transactions;
	}

}
