package com.ciaorentals.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
/**
 * This class implements the feedback.
 * @author ciaorentals
 *
 */
@Entity
@Table(name = "feedback_tbl")
public class FeedBack {
	//feedback table fields and setters and getters are initialized.
	@Id
	@GenericGenerator(name = "IdGen" ,strategy = "com.ciaorentals.util.IdGenerator" )
	@GeneratedValue(generator = "IdGen")
	@Column
	private int feedback_id;
	@Column
	private String comments;
	
	@ManyToOne
	private UserEntity user;
	
	
	public UserEntity getUser() {
		return user;
	}


	public void setUser(UserEntity user) {
		this.user = user;
	}


	public int getFeedback_id() {
		return feedback_id;
	}
 
	
	public void setFeedback_id(int feedback_id) {
		this.feedback_id = feedback_id;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	} 



}
