package com.ciaorentals.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
@Entity
@Table(name = "image_tbl")
public class ImagesEntity {
	@Id
	@GenericGenerator(name = "IdGen" ,strategy = "com.ciaorentals.util.IdGenerator" )
	@GeneratedValue(generator = "IdGen")

	@Column(name="image_id")
	private int imageId;
	@Column(name="image_name")
	private String imageName;
	public int getImageId() {
		return imageId;
	}
	public void setImageId(int imageId) {
		this.imageId = imageId;
	}
	public String getImageName() {
		return imageName;
	}
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	public UserEntity getUsers() {
		return users;
	}
	public void setUsers(UserEntity users) {
		this.users = users;
	}
	@ManyToOne
	private UserEntity users;
}
