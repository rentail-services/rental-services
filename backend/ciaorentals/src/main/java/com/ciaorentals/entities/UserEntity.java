package com.ciaorentals.entities;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "user_entity")
public class UserEntity {

	@Id
	@GenericGenerator(name = "IdGen", strategy = "com.ciaorentals.util.IdGenerator")
	@GeneratedValue(generator = "IdGen")
	@Column(name = "id")
	private int id;
	@Column(name = "name")
	private String name;
	@Column(name = "email_id")
	private String email;
	@Column(name = "date_of_birth")
	private Date dateOfBirth;
	@Column(name = "contact_number")
	private String contactNumber;
	@Column(name = "user_name")
	private String userName;
	@Column(name = "password")
	private String password;
	@Column(name = "gender")
	private String gender;

	@OneToMany
	private List<ImagesEntity> images;
	public List<ImagesEntity> getImages() {
		return images;
	}

	public void setImages(List<ImagesEntity> images) {
		this.images = images;
	}

	@OneToMany
	private List<ItemEntity> items;

	@OneToOne(mappedBy = "user", cascade = CascadeType.ALL)
	private WalletEntity wallet;
	
	@OneToMany
	private List<FeedBack> feedBack;
	
	public List<FeedBack> getFeedBack() {
		return feedBack;
	}

	public void setFeedBack(List<FeedBack> feedBack) {
		this.feedBack = feedBack;
	}

	public List<ItemEntity> getItems() {
		return items;
	}

	public void setItems(List<ItemEntity> items) {
		this.items = items;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public WalletEntity getWallet() {
		return wallet;
	}

	public void setWallet(WalletEntity wallet) {
		this.wallet = wallet;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

}
