package com.ciaorentals.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "transaction_entity")
public class TransactionEntity {

	
	@Id
	@GenericGenerator(name = "IdGen" ,strategy = "com.ciaorentals.util.IdGenerator")
	@GeneratedValue(generator = "IdGen")
	@Column(name = "id")
	private int id;
	@Column(name = "credit_amount")
	private double creditAmount;
	@Column(name = "debit_amount")
	private double debitAmount;
	@Column(name = "date_of_transaction")
	private Date dateOfTransaction;
	@ManyToOne
	private WalletEntity wallet;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getCreditAmount() {
		return creditAmount;
	}

	public void setCreditAmount(double creditAmount) {
		this.creditAmount = creditAmount;
	}

	public double getDebitAmount() {
		return debitAmount;
	}

	public void setDebitAmount(double debitAmount) {
		this.debitAmount = debitAmount;
	}

	public Date getDateOfTransaction() {
		return dateOfTransaction;
	}

	public void setDateOfTransaction(Date dateOfTransaction) {
		this.dateOfTransaction = dateOfTransaction;
	}

	public WalletEntity getWallet() {
		return wallet;
	}

	public void setWallet(WalletEntity wallet) {
		this.wallet = wallet;
	}

}
