package com.ciaorentals.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "items_entity")
public class ItemEntity {

	@Id
	@GenericGenerator(name = "IdGen", strategy = "com.ciaorentals.util.IdGenerator")
	@GeneratedValue(generator = "IdGen")
	@Column(name = "id")
	private int id;
	@Column(name = "category")
	private String category;
	@Column(name = "brand_name")
	private String brand;
	@Column(name = "model_name")
	private String model;
	@Column(name = "place")
	private String place;
	@Column(name = "rent_per_day")
	private int rentPerDay;
	@Column(name = "item_status")
	private String status;
	@Column(name = "available_up_to")
	private Date availableUpTo;
	@Column(name = "type")
	private String type;
	@Column(name = "images")
	private String images;

//	@OneToMany(mappedBy = "items")

	@ManyToOne
	private UserEntity user;

	public String getImages() {
		return images;
	}

	public void setImages(String images) {
		this.images = images;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public int getRentPerDay() {
		return rentPerDay;
	}

	public void setRentPerDay(int rentPerDay) {
		this.rentPerDay = rentPerDay;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getAvailableUpTo() {
		return availableUpTo;
	}

	public void setAvailableUpTo(Date availableUpTo) {
		this.availableUpTo = availableUpTo;
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}