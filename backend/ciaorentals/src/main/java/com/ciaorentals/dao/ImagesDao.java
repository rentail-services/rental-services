package com.ciaorentals.dao;


import org.springframework.data.repository.CrudRepository;

import com.ciaorentals.entities.ImagesEntity;

public interface ImagesDao extends CrudRepository<ImagesEntity, Integer>  {

}
