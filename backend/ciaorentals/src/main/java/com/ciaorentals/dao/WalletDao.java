package com.ciaorentals.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ciaorentals.entities.WalletEntity;
@Repository
public interface WalletDao extends JpaRepository<WalletEntity, Integer> {
	WalletEntity findById(int id);
}
