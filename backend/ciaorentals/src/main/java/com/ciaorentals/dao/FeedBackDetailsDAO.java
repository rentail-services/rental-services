package com.ciaorentals.dao;

import org.springframework.data.repository.CrudRepository;

import com.ciaorentals.entities.FeedBackDetails;

/**
 * this interface used to declare the methods to perform database operations on
 * FeedBackDetails table
 * 
 * @author IMVIZAG
 *
 */
public interface FeedBackDetailsDAO extends CrudRepository<FeedBackDetails, Integer> {

}
