package com.ciaorentals.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ciaorentals.entities.ItemEntity;


public interface ItemDao extends JpaRepository<ItemEntity, Integer> {
	@Query(value = "select * from items_entity where user_id = ?", nativeQuery = true)
	public List<ItemEntity> findByUserId(int id);

	public ItemEntity findById(int itemId);

	@Query(value = "select * from items_entity where type=?", nativeQuery = true)
	public List<ItemEntity> findByCategory(String type);

	
}
