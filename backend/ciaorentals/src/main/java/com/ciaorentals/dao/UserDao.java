package com.ciaorentals.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ciaorentals.entities.FeedBack;
import com.ciaorentals.entities.UserEntity;
/**
 * This is a interface for UserDAO
 * @author IMVIZAG
 *
 */
@Repository
public interface UserDao extends JpaRepository<UserEntity, Integer> {
	UserEntity findById(int id);
	/**
	 * this method is used to check whether the email is existing or not.
	 * @param email
	 * @return
	 */
	public UserEntity findByEmail(String email);
	

	
	
}
