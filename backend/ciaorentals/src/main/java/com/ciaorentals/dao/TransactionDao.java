package com.ciaorentals.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ciaorentals.entities.TransactionEntity;
@Repository
public interface TransactionDao extends JpaRepository<TransactionEntity, Integer> {
	@Query(value = "select * from transaction_entity where wallet_id = ?", nativeQuery = true)
	TransactionEntity checkUser(int id);
	@Query(value = "select * from transaction_entity where user_id = ?", nativeQuery = true)
	TransactionEntity findByUser(int id);
	

}
