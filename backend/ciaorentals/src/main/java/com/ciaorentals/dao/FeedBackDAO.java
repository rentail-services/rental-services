package com.ciaorentals.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.ciaorentals.entities.FeedBack;

/**
 * this interface used to declare the methods to retrieve data from feedback
 * table
 * 
 * @author IMVIZAG
 *
 */
public interface FeedBackDAO extends CrudRepository<FeedBack, Integer> {
	@Query(value = "select * from feedback_tbl where user_id = ?", nativeQuery = true)
	public List<FeedBack> feedbacklist(int id);
}
